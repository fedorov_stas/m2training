<?php

namespace Pulsestorm\ToDoCrud\Model\ResourceModel;

use \Magento\Framework\Model\ResourceModel\Db\Context;
use \Magento\Framework\EntityManager\EntityManager;

class TodoItem extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected $entityManager;

    public function __construct(Context $context, EntityManager $entityManager, $connectionName = null)
    {
        $this->entityManager = $entityManager;
        parent::__construct($context, $connectionName);
    }

    protected function _construct()
    {
        $this->_init('pulsestorm_todocrud_todoitem', 'pulsestorm_todocrud_todoitem_id');
    }

    public function load(\Magento\Framework\Model\AbstractModel $object, $value, $field = null)
    {
        $this->entityManager->load($object, $value);
    }

    public function save(\Magento\Framework\Model\AbstractModel $object)
    {
        $this->entityManager->save($object);
    }

    public function delete(\Magento\Framework\Model\AbstractModel $object)
    {
        $this->entityManager->delete($object);
    }
}
