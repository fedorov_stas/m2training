<?php

namespace Pulsestorm\ToDoCrud\Setup;

class InstallData implements \Magento\Framework\Setup\InstallDataInterface
{
    private $toDoItemFactory;

    public function __construct(\Pulsestorm\ToDoCrud\Model\ToDoItemFactory $todoItemFactory)
    {
        $this->toDoItemFactory = $todoItemFactory;
    }

    public function install(
        \Magento\Framework\Setup\ModuleDataSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        $toDoItemList = [
            [
                'title' => 'title 1',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent iaculis nisl interdum, suscipit massa a, tincidunt lacus. Nulla laoreet nisl a imperdiet suscipit. In rhoncus, purus ac ultricies accumsan, dui ligula auctor mi, at finibus risus odio in magna. Suspendisse finibus tempus diam vel posuere. Sed ultricies blandit massa quis fermentum. Fusce posuere ornare gravida. Donec viverra lorem ut arcu blandit, vel placerat felis auctor.

Integer hendrerit lorem elit, at placerat massa maximus ac. In ipsum nibh, hendrerit eu vulputate eget, maximus sit amet mauris. Nulla mauris urna, ornare ac massa ut, interdum semper libero. Suspendisse quis leo sit amet ante tempor sollicitudin. Duis eget nunc id orci mattis accumsan non eget orci. Vivamus sodales ut augue ac vestibulum. Donec lacinia scelerisque elit a efficitur. Vestibulum scelerisque orci sed malesuada ultricies. Mauris ultrices eu tellus nec pellentesque. Aliquam porta quam eget pretium viverra.

Donec in cursus mi. Sed cursus diam vitae orci efficitur, vitae finibus dui pulvinar. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin turpis tortor, venenatis at feugiat a, varius lobortis magna. Phasellus eget augue ornare, convallis sem faucibus, viverra risus. Integer consequat orci eget ornare viverra. Morbi vestibulum at nisi sed convallis. Vivamus condimentum lorem nisi, at venenatis turpis egestas sed. Sed sed tincidunt leo, at auctor leo. Vivamus lacinia magna in sodales consequat.',
                'is_active' => 1
            ],
            [
                'title' => 'title 2',
                'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent iaculis nisl interdum, suscipit massa a, tincidunt lacus. Nulla laoreet nisl a imperdiet suscipit. In rhoncus, purus ac ultricies accumsan, dui ligula auctor mi, at finibus risus odio in magna. Suspendisse finibus tempus diam vel posuere. Sed ultricies blandit massa quis fermentum. Fusce posuere ornare gravida. Donec viverra lorem ut arcu blandit, vel placerat felis auctor.

Integer hendrerit lorem elit, at placerat massa maximus ac. In ipsum nibh, hendrerit eu vulputate eget, maximus sit amet mauris. Nulla mauris urna, ornare ac massa ut, interdum semper libero. Suspendisse quis leo sit amet ante tempor sollicitudin. Duis eget nunc id orci mattis accumsan non eget orci. Vivamus sodales ut augue ac vestibulum. Donec lacinia scelerisque elit a efficitur. Vestibulum scelerisque orci sed malesuada ultricies. Mauris ultrices eu tellus nec pellentesque. Aliquam porta quam eget pretium viverra.

Donec in cursus mi. Sed cursus diam vitae orci efficitur, vitae finibus dui pulvinar. Interdum et malesuada fames ac ante ipsum primis in faucibus. Proin turpis tortor, venenatis at feugiat a, varius lobortis magna. Phasellus eget augue ornare, convallis sem faucibus, viverra risus. Integer consequat orci eget ornare viverra. Morbi vestibulum at nisi sed convallis. Vivamus condimentum lorem nisi, at venenatis turpis egestas sed. Sed sed tincidunt leo, at auctor leo. Vivamus lacinia magna in sodales consequat.',
                'date_completed' => date('Y-m-d H:i:s', strtotime('+1 month')),
                'is_active' => 1
            ]
        ];

        foreach ($toDoItemList as $toDoItem) {
            $this->toDoItemFactory->create()->setData($toDoItem)->save();
        }
    }
}
