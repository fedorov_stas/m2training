<?php

namespace Pulsestorm\ToDoCrud\Block;

use Magento\Framework\View\Element\Template;
use Pulsestorm\ToDoCrud\Model\TodoItemFactory;
use Pulsestorm\ToDoCrud\Model\ResourceModel\TodoItem as TodoItemResource;

class Main extends \Magento\Framework\View\Element\Template
{
    protected $toDoItemFactory;
    protected $todoItemResource;

    public function __construct(Template\Context $context, TodoItemFactory $toDoItemFactory, TodoItemResource $todoItemResource)
    {
        $this->toDoItemFactory = $toDoItemFactory;
        $this->todoItemResource = $todoItemResource;

        parent::__construct($context);
    }

    function _prepareLayout()
    {
        $todo = $this->toDoItemFactory->create();

//        item creation
        $todo->setTitle('title 4');
        $todo->setDescription('description 4');
        $this->todoItemResource->save($todo);

//        $todo = $todo->load(1);
//        var_dump($todo->getData());
//        var_dump($todo->getTitle());
//        var_dump($todo->getDescription());
//        var_dump($todo->getData('title'));
//        var_dump($todo->getData('description'));
        exit;
    }
}
