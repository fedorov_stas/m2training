<?php

namespace Pulsestorm\RepositoryTutorial\Command;

use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\App\State;
use Magento\Framework\ObjectManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class Examples extends Command
{
    protected $objectManager;
    protected $helper;

    public function __construct(
        ObjectManagerInterface $objectManager,
        State $appState,
        \Pulsestorm\RepositoryTutorial\Model\Helper $helper,
        $name = null
    )
    {
        $this->objectManager = $objectManager;
        $this->helper = $helper;
        $appState->setAreaCode('frontend');
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName("ps:examples");
        $this->setDescription("A command the programmer was too lazy to enter a description for.");
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        /** @var ProductRepository $productRepo */
        $productRepo = $this->objectManager->get('Magento\Catalog\Model\ProductRepository');
        $product = $productRepo->getById(3);
        echo get_class($product) . PHP_EOL;
        echo $product->getName() . PHP_EOL;

        $searchCriteria = $this->objectManager->create('Magento\Framework\Api\SearchCriteriaInterface');
        $productResult = $productRepo->getList($searchCriteria);
        $productList = $productResult->getItems();
        foreach ($productList as $product) {
            echo $product->getId() . ' => ' . $product->getName() . PHP_EOL;
        }

//        $pageRepo = $this->objectManager->get('Magento\Cms\Model\PageRepository');
//        $page = $pageRepo->getById(2);
//        echo $page->getTitle() . PHP_EOL;
//        $page->setTitle($page->getTitle() . ', edited by code!');
//        $pageRepo->save($page);
//
//        $page->setId(null);
//        $page->setTitle('My duplicated page!');
//        $pageRepo->save($page);
//        echo $page->getId() . PHP_EOL;
//        $pageRepo->delete($page);
    }
} 
