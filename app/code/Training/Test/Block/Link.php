<?php


namespace Training\Test\Block;

class Link extends \Magento\Framework\View\Element\Html\Link
{
    public function _toHtml()
    {
        return '<li class="' . $this->escapeHtml($this->getClass()) . '"><a ' . $this->getLinkAttributes() . '>' . $this->escapeHtml($this->getLabel()) . '</a></li>';
    }
}