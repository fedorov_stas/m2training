<?php

namespace Training\Test\Block;

class AlertActionName extends \Magento\Framework\View\Element\Template
{
    private $fullActionName;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Request\Http $request
    ) {
        $this->fullActionName = $request->getFullActionName();
        parent::__construct($context);
    }

    public function getFullActionName()
    {
        return $this->fullActionName;
    }
}