<?php

namespace Training\Test\Observer;

use Magento\Framework\App\ActionFlag;
use Magento\Framework\App\Response\RedirectInterface;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

class RedirectToProductView implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\Response\RedirectInterface
     */
    protected $redirect;

    /**
     * @var \Magento\Framework\App\ActionFlag
     */
    protected $_actionFlag;

    /**
     * RedirectToProductView constructor.
     * @param RedirectInterface $redirect
     * @param ActionFlag $actionFlag
     */
    public function __construct(
        RedirectInterface $redirect,
        ActionFlag $actionFlag
    ) {
        $this->redirect = $redirect;
        $this->_actionFlag = $actionFlag;
    }

    /**
     * @param Observer $observer
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute(Observer $observer)
    {
//        /** @var \Magento\Framework\App\Request\Http $request */
//        $request = $observer->getEvent()->getData('request');
//        if ($request->getModuleName() != 'catalog' || $request->getControllerName() != 'product') {
//            /** @var \Magento\Framework\App\Action\Action $controller */
//            $controller = $observer->getControllerAction();
//            $this->_actionFlag->set('', \Magento\Framework\App\Action\Action::FLAG_NO_DISPATCH, true);
//            $this->redirect->redirect($controller->getResponse(), 'catalog/product/view/id/1');
//        }
    }
}