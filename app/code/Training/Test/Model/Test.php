<?php
/**
 * Created by PhpStorm.
 * User: preacher
 * Date: 16.04.18
 * Time: 22:58
 */

namespace Training\Test\Model;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Checkout\Model\Session;

class Test
{
    protected $productRepository;
    protected $productFactory;
    protected $session;
    protected $justAParameter;
    protected $data;

    public function __construct(
        ProductRepositoryInterface $productRepository,
        ProductFactory $productFactory,
        Session $session,
        $justAParameter = false,
        array $data
    ) {
        $this->productRepository = $productRepository;
        $this->productFactory = $productFactory;
        $this->session = $session;
        $this->justAParameter = $justAParameter;
        $this->data = $data;
    }

    public function log()
    {
        var_dump($this->justAParameter);
        var_dump($this->data);
    }
}