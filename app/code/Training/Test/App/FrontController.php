<?php

namespace Training\Test\App;


use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Response\Http;
use Magento\Framework\App\RouterList;
use Magento\Framework\App\RouterListInterface;
use Magento\Framework\Logger\Monolog;

class FrontController extends \Magento\Framework\App\FrontController
{
    /** @var RouterList */
    protected $routerList;
    /** @var Http */
    protected $response;
    /** @var Monolog */
    protected $logger;

    /**
     * FrontController constructor.
     * @param RouterListInterface $routerList
     * @param Http $response
     * @param Monolog $logger
     */
    public function __construct(RouterListInterface $routerList, Http $response, Monolog $logger)
    {
        $this->routerList = $routerList;
        $this->response = $response;
        $this->logger = $logger;

        parent::__construct($routerList, $response);
    }

    /**
     * @param RequestInterface $request
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function dispatch(RequestInterface $request)
    {
        foreach ($this->routerList as $router) {
            $this->logger->addDebug(get_class($router));
        }

        return parent::dispatch($request);
    }
}