<?php

namespace Training\Test\Controller\Adminhtml\Action;

use Magento\Backend\App\Action;

class Index extends Action
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        $this->getResponse()->appendBody("HELLO WORLD");

        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        // стандартный редирект работает только для админки
        // в методе Store::getUrl бага https://github.com/magento/magento2/issues/5322
        $this->_redirect($objectManager->get('Magento\Store\Model\StoreManagerInterface')->getStore(1)->getBaseUrl() . 'catalog/category/view/id/3');
    }

    /**
     * Check if admin has permissions to visit related pages
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        $secret = $this->getRequest()->getParam('secret');
        return isset($secret) && (int)$secret == 1;
    }
}