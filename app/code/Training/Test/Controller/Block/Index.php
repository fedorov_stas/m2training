<?php

namespace Training\Test\Controller\Block;


use Magento\Framework\App\Action\Action;

class Index extends Action
{
    public function execute()
    {
        $layout = $this->_view->getLayout();
        $block = $layout->createBlock('Training\Test\Block\Test');
        $this->getResponse()->appendBody($block->toHtml());
    }
}