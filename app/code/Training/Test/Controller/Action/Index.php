<?php

namespace Training\Test\Controller\Action;

use Magento\Framework\App\Action\Action;

class Index extends Action
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface|void
     */
    public function execute()
    {
        /** @var \Training\Test\Block\Template $block */
        $block = $this->_view->getLayout()->addBlock('Training\Test\Block\Template');
        $block->setTemplate('Training_Test::test.phtml');
        $this->getResponse()->appendBody($block->toHtml());
    }

}