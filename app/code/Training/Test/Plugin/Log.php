<?php

namespace Training\Test\Plugin;


/**
 * Class Log
 * @package Training\Test\Plugin
 */
class Log
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * Log constructor.
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(\Psr\Log\LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param \Magento\Customer\Model\Customer\DataProvider $subject
     * @param $result
     */
    public function afterGetData(\Magento\Customer\Model\Customer\DataProvider $subject, $result)
    {
        $this->logger->info(print_r($result, true));
    }
}