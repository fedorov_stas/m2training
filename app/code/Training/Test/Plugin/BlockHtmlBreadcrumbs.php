<?php

namespace Training\Test\Plugin;


class BlockHtmlBreadcrumbs
{
    public function beforeAddCrumb(\Magento\Theme\Block\Html\Breadcrumbs $subject, $crumbName, $crumbInfo)
    {
        // 1.7.2: Plugins 2
        // приватные свойства не модифицируются
        return [$crumbName, $crumbInfo];
    }
}