<?php

namespace Training\Test\Plugin;


class BlockHtmlFooter
{
    public function afterGetCopyright(\Magento\Theme\Block\Html\Footer $subject)
    {
        return "Customized copyright!";
    }
}