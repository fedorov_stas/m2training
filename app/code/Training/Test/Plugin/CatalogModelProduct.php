<?php

namespace Training\Test\Plugin;


class CatalogModelProduct
{
    public function afterGetPrice(\Magento\Catalog\Model\Product $subject, $result)
    {
        return $result * 2;
    }
}