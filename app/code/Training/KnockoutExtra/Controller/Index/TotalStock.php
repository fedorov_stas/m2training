<?php

namespace Training\KnockoutExtra\Controller\Index;

use Magento\CatalogInventory\Api\StockStatusCriteriaInterfaceFactory;
use Magento\CatalogInventory\Api\StockStatusRepositoryInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class TotalStock extends \Magento\Framework\App\Action\Action
{

    protected $resultJsonFactory;
    protected $stockStatusRepository;
    protected $criteriaInterfaceFactory;

    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        StockStatusRepositoryInterface $stockStatusRepository,
        StockStatusCriteriaInterfaceFactory $criteriaInterfaceFactory
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->stockStatusRepository = $stockStatusRepository;
        $this->criteriaInterfaceFactory = $criteriaInterfaceFactory;
    }

    public function execute()
    {
        $stockStatusList = $this->stockStatusRepository->getList($this->criteriaInterfaceFactory->create())->getItems();

        $totalStock = 0;
        foreach ($stockStatusList as $stockStatus) {
            $totalStock += $stockStatus->getQty();
        }

        $result = $this->resultJsonFactory->create();
        $result->setData(['totalStock' => $totalStock]);

        return $result;
    }
}
