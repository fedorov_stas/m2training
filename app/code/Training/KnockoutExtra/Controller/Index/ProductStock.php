<?php

namespace Training\KnockoutExtra\Controller\Index;

use Magento\Catalog\Model\ProductRepository;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;
use Magento\Framework\Exception\NoSuchEntityException;

class ProductStock extends \Magento\Framework\App\Action\Action
{

    protected $resultJsonFactory;
    protected $stockState;
    protected $productRepository;

    public function __construct(
        Context $context,
        JsonFactory $resultJsonFactory,
        StockStateInterface $stockState,
        ProductRepository $productRepository
    ) {
        parent::__construct($context);
        $this->resultJsonFactory = $resultJsonFactory;
        $this->stockState = $stockState;
        $this->productRepository = $productRepository;
    }

    public function execute()
    {
        $result = $this->resultJsonFactory->create();

        try {
            $product = $this->productRepository->getById((int)$this->getRequest()->getParam('productId'));
        } catch (NoSuchEntityException $exception) {
            $result->setData(['status' => false]);
            return $result;
        }

        $result->setData([
            'status' => true,
            'productName' => $product->getName(),
            'productStock' => $this->stockState->getStockQty($product->getId())
        ]);
        return $result;
    }
}
