define([
    'jquery',
    'uiComponent',
    'ko',
    'mage/translate',
], function ($, Component, ko) {
    'use strict';
    return Component.extend({
        defaults: {
            showProductStockDefault: false,
        },
        showProductStock: ko.observable(false),
        totalStock: ko.observable(null),
        productId: ko.observable(null),
        productStock: ko.observable(null),
        productName: ko.observable(null),

        /** @inheritdoc */
        initialize: function () {
            this.showProductStock(this.showProductStockDefault);
            return this;
        },
        getShowHideButtonTemplate: function () {
            return 'Training_KnockoutExtra/show_hide_button';
        },
        clickShowHideButton: function () {
            this.showProductStock(!this.showProductStock());
        },
        getProductStockTemplate: function () {
            return 'Training_KnockoutExtra/product_stock';
        },
        getTotalStock: function () {
            $.get('/ko_extra/index/totalstock', $.proxy(function (data) {
                this.totalStock(data.totalStock);
            }, this));

            var totalStock = this.totalStock() != null && parseInt(this.totalStock()) > 0 ? this.totalStock() : 0;
            return $.mage.__('Общий остаток на складе: %1').replace('%1', totalStock);
        },
        getProductStock: function () {
            if (this.productId()) {
                $.get('/ko_extra/index/productstock', {productId: this.productId}, $.proxy(function (data) {
                    if (data.status) {
                        this.productStock(data.productStock);
                        this.productName(data.productName);
                    } else {
                        this.productStock(null);
                        this.productName(null);
                    }
                }, this));
            }
        }
    });
});
