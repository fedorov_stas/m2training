<?php

namespace Training\HwUnit4\Block;

use Magento\Catalog\Api\CategoryListInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Catalog\Model\Category as CategoryModel;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\Search\FilterGroupBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Store\Model\ResourceModel\Store\Collection as StoreCollection;
use Magento\Store\Model\Store;
use Magento\Store\Model\StoreManagerInterface;

class Main extends Template
{
    protected $storeCollection;
    protected $store;
    protected $categoryList;
    protected $categoryRepository;
    protected $searchCriteriaBuilder;
    protected $filterBuilder;
    protected $filterGroupBuilder;
    protected $storeManager;

    public function __construct(
        StoreManagerInterface $storeManager,
        StoreCollection $storeCollection,
        Store $store,
        Context $context,
        CategoryListInterface $categoryList,
        CategoryRepositoryInterface $categoryRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        FilterGroupBuilder $filterGroupBuilder,
        array $data = []
    )
    {
        $this->storeManager = $storeManager;
        $this->storeCollection = $storeCollection;
        $this->store = $store;
        $this->categoryList = $categoryList;
        $this->categoryRepository = $categoryRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->filterGroupBuilder = $filterGroupBuilder;
        parent::__construct($context, $data);
    }

    function _prepareLayout()
    {
        var_dump($this->storeCollection->getAllIds());
        var_dump($this->store->getRootCategoryId());

        $filter = $this->filterBuilder
            ->setField(CategoryModel::KEY_PARENT_ID)
            ->setValue(CategoryModel::TREE_ROOT_ID)
            ->setConditionType('eq')
            ->create();

        $searchCriteria = $this->searchCriteriaBuilder->addFilters([$filter])->create();

        $categoryList = $this->categoryList->getList($searchCriteria)->getItems();

        foreach ($categoryList as $category) {
            echo $category->getId() . ' => ' . $category->getName() . '<br>';
        }

        foreach ($this->storeManager->getGroups() as $storeGroup) {
            echo $storeGroup->getName() . ' => ' . $this->categoryRepository->get($storeGroup->getRootCategoryId())->getName() . '<br>';
        }
    }
}
