<?php


namespace Training\HwUnit4\Observer;

use Magento\Catalog\Model\Product;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Setup\Exception;
use Psr\Log\LoggerInterface;

/**
 * Class CatalogProductSaveAfterLog
 * @package Training\HwUnit4\Observer
 */
class CatalogProductSaveBeforeLog implements ObserverInterface
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * CatalogProductSaveAfterLog constructor.
     *
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        /** @var Product $product */
        $product = $observer->getProduct();

        if ($product->hasDataChanges()) {
            $log = "{$product->getId()} product data has changes";
            $this->logger->info($log);
        }
    }
}
